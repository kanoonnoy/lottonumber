﻿using LigorLottoNumber.Base;
using LigorLottoNumber.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber
{
    public partial class MainParentForm : Form
    {
        private MdiClient mdiClient = null;
        private int childFormNumber = 0;
        public MainParentForm()
        {
            InitializeComponent();
            //navigationPane1.SelectedPanel = navigationPane1.TitlePanel[];// Navigator Bar

            IsMdiContainer = true; // MDI

            treeView1.ExpandAll();

#if DEBUG
           // Properties.Settings.Default["LigorLottoConnectionString"] = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\DataBase\\LigorLotto.mdb";
#endif
            toolStripStatusLabel.Text = string.Format("ชื่อเครื่อง: {0} , เข้าใช้งานเมื่อ {1}" ,Environment.MachineName,DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
        }



        private void MenuNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string nodeText;
            string nodeTag;
            string nodeCustomization;
            string nodeToolTipText;
            string formTitle;
            bool newWindows;

            TreeView treeView;
            treeView = (TreeView)sender;
            if (e.Node.Nodes.Count == 0 && treeView.SelectedNode.Text == e.Node.Text)
            {
                nodeTag = e.Node.Tag == null ? "" : e.Node.Tag.ToString();
                nodeText = e.Node.Text;
                nodeToolTipText = e.Node.ToolTipText;
                formTitle = e.Node.Parent == null ? nodeText : e.Node.Parent.Text + " - " + nodeText;
                newWindows = (e.Button == System.Windows.Forms.MouseButtons.Right);

                if (nodeTag != "" && nodeTag == "Shortcut")
                {
                    //Shortcut
                    Process proc = new Process();
                    proc.StartInfo.FileName = nodeToolTipText;
                    proc.StartInfo.Arguments = "";
                    proc.Start();
                }
                else if (nodeTag != "" && nodeTag != "Folder")
                {

                    int seperatePostion1 = nodeText.IndexOf(" R");
                    int seperatePostion2 = nodeText.IndexOf(" - ");

                    if (nodeTag.Substring(0, 2) == "RL")
                    {
                        //Dynamic Report
                        OpenReportForm(nodeText, nodeTag, "", newWindows);
                    }
                    else
                    {
                        //Normal ChildForm
                        // if (Global.CurrentUserPermission.CheckRead(nodeText))
                        {
                            string[] nodeTags = nodeTag.Split(';');
                            if (nodeTags.Length > 1)
                            {
                                nodeTag = nodeTags[0];
                                nodeCustomization = nodeTags[1];
                            }
                            else
                            {
                                nodeCustomization = "";
                            }

                            OpenChildFormClass(formTitle, nodeTag, nodeCustomization, newWindows);
                        }
                        //else
                        //    MessageBox.Show("ขออภัย! คุณไม่มีสิทธิ์ในการใช้งานเมนูนี้ค่ะ หากต้องการใช้งานกรุณาติดต่อผู้ดูแลระบบ", "สิทธิ์การใช้งาน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

            }

        }
        private bool IsChildFormNotExistingElseActive(string formname)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.Text == formname)
                {
                    f.Activate();
                    return false;
                }
            }
            return true;
        }
        private void ShowChildForm(string titile, ChildForm childForm, bool newWindow = false)
        {
            if (titile.Length > 0) childForm.Text = titile;
            if (!newWindow) childForm.MdiParent = this;
            childForm.WindowState = FormWindowState.Maximized;
            childForm.Show();
        }
        private void OpenChildFormClass(string ChildFormTitle, string ChildFormClass, string ChildFormCustomization = "", bool newWindow = false)
        {
            Type childFormType;
            ChildForm childForm;

            if (ChildFormClass.Length > 0)
            {
                if (IsChildFormNotExistingElseActive(ChildFormTitle) || ChildFormTitle.Length == 0)
                {
                    try
                    {
                        //if (!naviBar.Collapsed) naviBar.Collapsed = true;   

                        childFormType = Type.GetType(ChildFormClass);
                        childForm = (ChildForm)Activator.CreateInstance(childFormType);
                        childForm.FormCustomization = ChildFormCustomization;
                        if (ChildFormTitle.Length > 0) childForm.Text = ChildFormTitle;
                        if (!newWindow) childForm.MdiParent = this;
                        childForm.WindowState = FormWindowState.Maximized;
                        childForm.Show();
                        childForm.Activate();

                        Console.Beep(700, 50);
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.Message);
                        Debug.Print(ex.Message);
                    }
                }
            }
        }
        
        private void OpenReportForm(string ChildFormTitle, string reportNumber, string parameterFieldsText, bool newWindow = false, int entityID = -1)
        {
            if (reportNumber.Length > 0)
            {
                if (newWindow)
                {
                    ProcessStartInfo info = new ProcessStartInfo(System.Reflection.Assembly.GetEntryAssembly().Location);
                    //info.Arguments = "-reportviewer " + reportNumber + " " + Global.CurrentEntity.EntityID.ToString();
                    Process.Start(info);
                }
                else
                {
                    if (IsChildFormNotExistingElseActive(ChildFormTitle))
                    {
                        //if (!naviBar.Collapsed) naviBar.Collapsed = true;   

                        ReportLottoMasterForm childForm = new ReportLottoMasterForm(reportNumber, parameterFieldsText);
                        if (ChildFormTitle.Length > 0) childForm.Text = ChildFormTitle;
                        childForm.MdiParent = this;
                        childForm.WindowState = FormWindowState.Maximized;
                        childForm.Show();

                        Console.Beep(700, 50);
                    }
                }
            }
        }
        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }
        
        private void btAddLotto_Click(object sender, EventArgs e)
        {
            
            Lotto.LottoCreatorForm dlg = new Lotto.LottoCreatorForm();
            dlg.MdiParent = this;
            dlg.Show();
            dlg.WindowState = FormWindowState.Maximized;
        }

        private void ตงคาเลขอนToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lotto.LottoTempleteForm dlg = new Lotto.LottoTempleteForm();
            dlg.MdiParent = this;
            dlg.Show();
            dlg.WindowState = FormWindowState.Maximized;
        }
        
        private void MainParentForm_Load(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void เรมงวดใหมลางขอมลทงหมดToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการจะล้างข้อมูลทุกงวดหรือไม่?","ล้างข้อมูลหวย",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                foreach (Form childForm in MdiChildren)
                {
                    childForm.Close();
                }
                // Delete All item and Master
                int Deleteitem      = this.lottoItemTableAdapter1.DeleteAllItems();
                int DeleteDocument  = this.lottoMasterTableAdapter1.DeleteAll();
                
                MessageBox.Show("ล้างข้อมูลเรียบร้อยแล้ว!","ล้างฐานข้อมูล",MessageBoxButtons.OK,MessageBoxIcon.Information);


            }
        }

        private void ตงรหสผานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Admin.AdminSettingForm dlg = new Admin.AdminSettingForm();
            dlg.ShowDialog();
        }
    }
}
