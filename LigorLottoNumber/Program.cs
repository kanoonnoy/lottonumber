﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LigorLottoNumber
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [STAThread]
        static void Main()
        {
            bool result;
            var mutex = new System.Threading.Mutex(true, "UniqueAppId", out result);

            if (!result)
            {
                //MessageBox.Show("คุณได้เปิดโปรแกรมนี้ไปแล้ว");
                Process current = Process.GetCurrentProcess();
                foreach (Process process in Process.GetProcessesByName(current.ProcessName))
                {
                    if (process.Id != current.Id)
                    {
                        SetForegroundWindow(process.MainWindowHandle);
                        break;
                    }
                }
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GSBLicense.License lc = new GSBLicense.License();
            if (lc.CheckActivation())
            {
                Admin.LoinDialog dlg = new Admin.LoinDialog();
                if(dlg.ShowDialog() == DialogResult.Yes)
                    Application.Run(new MainParentForm());
            }

            GC.KeepAlive(mutex);
        }
    }
}
