﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Lotto
{
    public partial class LottoCreatorForm : Base.ChildForm
    {


        #region variable
        private bool isStartup = true;
        private DataTable dtTemplateMaster;
        public LottoCreatorForm()
        {
            InitializeComponent();
            comboBoxEx1.SelectedIndex = 0;
            comboBoxEx1.BackColor = Color.Tomato;
        }
        #endregion

        #region Spacial Keys
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.F1:
                    comboBoxEx1.SelectedIndex = 0;
                    comboBoxEx1.BackColor = Color.Tomato;
                    break;
                case Keys.F2:
                    comboBoxEx1.SelectedIndex = 1;
                    comboBoxEx1.BackColor = Color.Yellow;
                    break;
                case Keys.F3:
                    comboBoxEx1.SelectedIndex = 2;
                    comboBoxEx1.BackColor = Color.Green;
                    break;
                case Keys.F5:
                    UpdateAll();
                    Reload();
                    break;
            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void textBoxX1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void textBoxX1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
            }
        }
        #endregion

        #region Add LottoNumber
        struct LottoItems
        {
            public string GroupType;
            public int LottoBalanceAmount;
        }
        private void AddNewLottoNumber(int lottoid, string lottonumber, string type, int amount)
        {
            try
            {
                List<LottoItems> items = new List<LottoItems>();
                var resultSum = this.lottoItemTableAdapter.GetSumLottoAmount( lottonumber,this.dateTimeInput1.Value.Date);
                var SumCurrentLotto = 0;
                decimal Maxmimum = 0;
                if (resultSum == null) SumCurrentLotto = 0;
                else SumCurrentLotto = Convert.ToInt32(resultSum);
                var rowTemplate = dtTemplateMaster.Select(string.Format("LottoNumber = '{0}'", lottonumber));
                if (rowTemplate.Length > 0)
                {
                    Maxmimum = (decimal)rowTemplate[0]["MaximumAmount"];
                }

                if (SumCurrentLotto + amount < Maxmimum)
                {
                    items.Add(new LottoItems() { GroupType = "0", LottoBalanceAmount = amount });
                }
                else if (SumCurrentLotto + amount >= Maxmimum)
                {
                    if (SumCurrentLotto >= Maxmimum)
                    {
                        items.Add(new LottoItems() { GroupType = "1", LottoBalanceAmount = amount });
                    }
                    else
                    {
                        // Insert Group 1,2
                        var balance = Convert.ToInt32(Maxmimum - SumCurrentLotto);
                        var balance2 = Convert.ToInt32(amount - balance);
                        if(balance > 0) items.Add(new LottoItems() { GroupType = "0", LottoBalanceAmount = balance });
                        if (balance2 > 0) items.Add(new LottoItems() { GroupType = "1", LottoBalanceAmount = balance2 });
                    }
                }
                foreach (var item in items)
                {
                    var selectCase = string.Format("LottoID = {0} AND  LottoType ='{1}' AND LottoNumber = '{2}' AND PrintSequenceNumber = 0 AND GroupType='{3}'"
                        , lottoid, type, lottonumber,item.GroupType);
                    DataRow[] rowselect = this.ligorLottoDataSet.LottoItem.Select(selectCase);
                    if (rowselect.Length == 0)
                    {


                        // Add New
                        //// Get data table view 
                        DataView dataTableView = lottoItemBindingSource.List as DataView;
                        //// Create row from view
                        DataRowView rowView = dataTableView.AddNew();
                        rowView["LottoRewardDate"]  = this.dateTimeInput1.Value;
                        rowView["LottoID"]          = lottoid;
                        rowView["LottoType"]        = type;
                        rowView["LottoSequenceNumber"] = lottoItemBindingSource.Count;
                        rowView["LottoNumber"]      = lottonumber;
                        rowView["LottoAmount"]      = amount;
                        rowView["UpdateUser"]       = Environment.MachineName;
                        rowView["UpdateWhen"]       = DateTime.Now;

                        rowView["PrintSequenceNumber"] = 0;
                        rowView["GroupType"] = item.GroupType;
                        rowView["LottoBalanceAmount"] = item.LottoBalanceAmount;

                        this.lottoItemBindingSource.MoveLast();
                        this.lottoItemBindingSource.EndEdit();


                    }
                    else
                    {
                        var row = rowselect[0];
                        if (item.GroupType == row["GroupType"].ToString())
                        {
                            decimal oldAmount = ((decimal)row["LottoAmount"]);
                            decimal oldDisplay = ((decimal)row["LottoBalanceAmount"]);
                            row["LottoAmount"] = oldAmount + amount; // Update Amount
                            row["LottoBalanceAmount"] = oldDisplay +  item.LottoBalanceAmount;
                            this.lottoItemBindingSource.EndEdit();
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                lottoItemBindingSource.CancelEdit();
            }
            finally
            {

            }
        }
        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (this.lottoMasterBindingSource.Count == 0)
            {
                customerCodeTextBox.Focus();
                customerCodeTextBox.SelectAll();
                return;
            }
            if (integerInput1.Value <= 0)
            {
                integerInput1.Focus();
                return;
            }
            if (textBoxX1.TextLength != 3)
            {
                textBoxX1.Focus();
                return;
            }
            
            string lottoNumber = textBoxX1.Text;
            try
            {
                displaytextBox.Text = string.Empty;
                string displaytext = string.Empty;
                int input1 = integerInput1.Value;
                int input2 = integerInput2.Value;
                DataRowView rowView = (DataRowView)this.lottoMasterBindingSource.Current;
                int LottoID = (int)rowView["LottoID"];
                if (input2 == 0)
                {
                    AddNewLottoNumber(LottoID, lottoNumber, comboBoxEx1.Text, input1);
                    displaytext = string.Format("{0}={1:N0}", lottoNumber, input1);
                }
                else
                {
                    
                    List<string> list = new List<string>();
                    char[] lottoOriginal = lottoNumber.ToCharArray();

                    list.Add(lottoNumber);
                   // if (lottoOriginal[0] != lottoOriginal[1] && lottoOriginal[0] != lottoOriginal[2] && lottoOriginal[1] != lottoOriginal[2])
                    {
                        list.Add(string.Format("{0}{1}{2}", lottoOriginal[0], lottoOriginal[2], lottoOriginal[1]));

                        list.Add(string.Format("{0}{1}{2}", lottoOriginal[1], lottoOriginal[0], lottoOriginal[2]));
                        list.Add(string.Format("{0}{1}{2}", lottoOriginal[1], lottoOriginal[2], lottoOriginal[0]));

                        list.Add(string.Format("{0}{1}{2}", lottoOriginal[2], lottoOriginal[0], lottoOriginal[1]));
                        list.Add(string.Format("{0}{1}{2}", lottoOriginal[2], lottoOriginal[1], lottoOriginal[0]));

                    }
                   
                    list = list.Distinct().ToList();
                    for(int i = 0; i < list.Count(); i ++)
                    {
                        if (list[i] == lottoNumber)
                        {
                            AddNewLottoNumber(LottoID, list[i], comboBoxEx1.Text, input1);
                            displaytext += (displaytext.Length > 0 ? " + " : "") + string.Format("{0}={1:N0}", list[i], input1);
                        }
                        else
                        {
                            AddNewLottoNumber(LottoID, list[i], comboBoxEx1.Text, input2);
                            displaytext += (displaytext.Length > 0 ? " + " : "") + string.Format("{0}={1:N0}", list[i], input2);
                        }
                    }

                }
                if (displaytext.Length > 0)
                    displaytextBox.Text = displaytext;



                CalculateAmount();



                UpdateAll();


                
                textBoxX1.Clear();

                integerInput1.ValueObject = null;
                integerInput2.ValueObject = null;


                Application.DoEvents();


                textBoxX1.Focus();
                textBoxX1.SelectAll();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }
        }
        private void integerInput2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                buttonX1_Click(e, new EventArgs());
        }
        private void CalculateAmount()
        {
            this.integerInput3.ValueObject = null;

            this.doubleInput1.ValueObject = null;
            this.doubleInput2.ValueObject = null;

            if (this.dataGridViewX2.Rows.Count == 0) return;

            try
            {
                double TotalAmount = 0;
                double TotalAmount2 = 0;
                double SumAmount = 0;
                if(this.dataGridViewX2.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in this.dataGridViewX2.Rows)
                    {
                        TotalAmount += Convert.ToDouble(row.Cells["dg_LottoAmount"].Value);
                    }
                }
                
                if(this.dataGridViewX3.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in this.dataGridViewX3.Rows)
                    {
                        TotalAmount2 += Convert.ToDouble(row.Cells["dg_LottoAmount2"].Value);
                    }
                }
               

                SumAmount = TotalAmount + TotalAmount2;

                this.doubleInput1.Value = TotalAmount;
                this.doubleInput2.Value = TotalAmount2;
                this.integerInput3.Value = SumAmount;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }
        private void lottoMasterBindingSource_PositionChanged(object sender, EventArgs e)
        {
            CalculateAmount();
        }
        #endregion

        #region Update

        private void UpdateAll()
        {

            try
            {
                this.panelEx1.Enabled = false;
                this.Cursor = Cursors.WaitCursor;


                this.Validate();
                DataTable dtMaster = this.ligorLottoDataSet.LottoMaster.GetChanges();
                DataTable dtitem = this.ligorLottoDataSet.LottoItem.GetChanges();
                this.lottoMasterBindingSource.EndEdit();
                this.lottoItemBindingSource.EndEdit();

                int UpdateRows = this.tableAdapterManager.UpdateAll(this.ligorLottoDataSet);
                Console.WriteLine("Update Rows {0}", UpdateRows);

                //ligorLottoDataSet.AcceptChanges();

                if (dtMaster != null)
                {
                    // Reload All
                    this.ligorLottoDataSet.LottoItem.Clear();
                    this.ligorLottoDataSet.LottoMaster.Clear();

                    this.lottoMasterTableAdapter.Fill(this.ligorLottoDataSet.LottoMaster);
                    this.lottoItemTableAdapter.Fill(this.ligorLottoDataSet.LottoItem);
                }
                else if (dtitem != null)
                {
                    this.lottoItemTableAdapter.Fill(ligorLottoDataSet.LottoItem);
                }

                CalculateAmount();

            }
            catch (DBConcurrencyException ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("ขออภัย ระบบบันทึกข้อมูลใหม่สำเร็จกำลังโหลดข้อมูลล่าสุดค่ะ!","บันทึกข้อมูลไม่สำเร็จ",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                Reload();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.panelEx1.Enabled = true;
                this.Cursor = Cursors.Default;
            }


        }
        private void lottoMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            UpdateAll();
            Reload();
        }
        private void dataGridViewX1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            UpdateAll();
        }

        private void dataGridViewX2_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            UpdateAll();
        }

        private void dataGridViewX2_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridViewX2.SelectedRows.Count == 0) return;

                e.Cancel = true;
                RemoveLottoNumberDialog dlg = new RemoveLottoNumberDialog();
                var row = dataGridViewX2.SelectedRows;
                string number = row[0].Cells["dg_LottoNumber"].Value.ToString();
                int amount = Convert.ToInt32(row[0].Cells["dg_LottoAmount"].Value.ToString());

                dlg.LottoAmount = amount;
                dlg.LottoNumber = number;

                if(dlg.ShowDialog() == DialogResult.Yes)
                {
                    if (dlg.LottoAmountEdited == 0)
                        e.Cancel = false;
                    else
                    {
                        // update
                        row[0].Cells["dg_LottoAmount"].Value = dlg.LottoAmountEdited;
                        UpdateAll();

                    }
                }


                //if (MessageBox.Show(string.Format("คุณต้องการลบตัวเลข '{0}' เป็นจำนวนเงิน {1:N0} บาท หรือไม่?", number, amount), "ลบตัวเลขชุดนี้", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                //{
                //    e.Cancel = true;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
            }
           
        }

        private void btEditLottoAmount_Click(object sender, EventArgs e)
        {
            if(dataGridViewX2.SelectedRows.Count > 0)
            {

                RemoveLottoNumberDialog dlg = new RemoveLottoNumberDialog();
                var row = dataGridViewX2.SelectedRows;
                string number = row[0].Cells["dg_LottoNumber"].Value.ToString();
                int amount = Convert.ToInt32(row[0].Cells["dg_LottoAmount"].Value.ToString());

                dlg.LottoAmount = amount;
                dlg.LottoNumber = number;

                if (dlg.ShowDialog() == DialogResult.Yes)
                {
                    if (dlg.LottoAmountEdited == 0)
                        dataGridViewX2.Rows.RemoveAt(dataGridViewX2.SelectedRows[0].Index);
                    else
                    {
                        // update
                        row[0].Cells["dg_LottoAmount"].Value = dlg.LottoAmountEdited;
                    }
                    UpdateAll();
                }

            }
               
        }
        #endregion

        #region Reload
        private void Reload()
        {
            try
            {
                this.panelEx1.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                if (isStartup)
                {
#if DEBUG
                   // Properties.Settings.Default["LigorLottoConnectionString"] = Properties.Settings.Default.LigorLottoConnectionString.Replace("|DataDirectory|", "D:\\DataBase");
#endif
                    dateTimeInput1.MinDate = DateTime.Now.Date;
                    dateTimeInput1.MaxDate = DateTime.Now.AddMonths(2);

                    DateTime Preiod1 = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);
                    DateTime Preiod2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
                    if (DateTime.Now.Day > 16)
                        dateTimeInput1.Value = Preiod1;
                    else
                        dateTimeInput1.Value = Preiod2;

                    this.checkBox1.Checked = false;
                    RelinkDisplayLotto();


                    timer1.Enabled = true;
                    isStartup = false;

                }

                this.ligorLottoDataSet.LottoItem.Clear();
                this.ligorLottoDataSet.LottoMaster.Clear();

                this.lottoMasterTableAdapter.Fill(this.ligorLottoDataSet.LottoMaster);
                this.lottoItemTableAdapter.Fill(this.ligorLottoDataSet.LottoItem);

                this.lottoItemBindingSource.Sort = "LottoNumber ASC";
                this.bindingSource1.Sort = "LottoNumber ASC";
                this.lottoMasterBindingSource.Sort = "LottoID DESC";

                dtTemplateMaster = this.lottoTemplateTableAdapter1.GetData();

                this.lottoMasterBindingSource.MoveFirst();
                this.lottoItemBindingSource.MoveFirst();


            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.panelEx1.Enabled = true;
                this.Cursor = Cursors.Default;
            }

        }
        private void RelinkDisplayLotto()
        {
            if (this.checkBox1.Checked)
            {
                this.lottoItemBindingSource.Filter = "GroupType='0' AND PrintSequenceNumber = 0";
                this.bindingSource1.Filter = "GroupType='1' AND PrintSequenceNumber = 0";
            }
            else
            {
                this.lottoItemBindingSource.Filter = "GroupType='0'";
                this.bindingSource1.Filter = "GroupType='1'";
            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            RelinkDisplayLotto();
        }

        private void LottoCreatorForm_Load(object sender, EventArgs e)
        {
            Reload();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.TimeOfDay < new TimeSpan(12, 0, 0))
                labelX5.ForeColor = Color.DarkGreen;
            else if (DateTime.Now.TimeOfDay < new TimeSpan(13, 0, 0))
                labelX5.ForeColor = Color.GreenYellow;
            else if (DateTime.Now.TimeOfDay < new TimeSpan(13, 30, 0))
                labelX5.ForeColor = Color.OrangeRed;
            else
                labelX5.ForeColor = Color.Red;


            labelX5.Text = DateTime.Now.ToString("HH:mm:ss");
        }
        #endregion

        #region Add Customer
        private void btAddCustomer_Click(object sender, EventArgs e)
        {
            AddCustomer();
        }
       
        private void AddCustomer()
        {
            if (customerCodeTextBox.TextLength == 0) return;

            try
            {
                string customercode = customerCodeTextBox.Text;
                string NumberLotto = dateTimeInput1.Value.Day < 15 ? "1" : "2";
                int Count = lottoMasterBindingSource.Count + 1;
                string number = Count.ToString();
                while (number.Length < 4) number = "0" + number;
                string LottoDocumentNumber = string.Format("L{0}{1}{2}", dateTimeInput1.Value.ToString("yyMM"), NumberLotto, number);

                //// Get data table view 
                DataView dataTableView = lottoMasterBindingSource.List as DataView;
                //// Create row from view
                DataRowView rowView = dataTableView.AddNew();

                rowView["LottoRewardDate"] = dateTimeInput1.Value.Date;
                rowView["LottoIssueWhen"] = DateTime.Now;
                rowView["LottoDocumentNumber"] = LottoDocumentNumber;
                rowView["CustomerCode"] = customercode;

                //rowView["Amount"] = "";
                //rowView["Remark"] = "";

                rowView["UpdateUser"] = Environment.MachineName;
                rowView["UpdateWhen"] = DateTime.Now;

                lottoMasterBindingSource.MoveLast();
                lottoMasterBindingSource.EndEdit();

                UpdateAll();
                Reload();

                customerCodeTextBox.Clear();
                textBoxX1.Focus();
                textBoxX1.SelectAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.Message);
                lottoItemBindingSource.CancelEdit();
            }
            finally
            {

            }
        }
        private void customerCodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                AddCustomer();
            }
        }
        #endregion

        #region Relink
        private void dateTimeInput1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimeInput1.Value.Date >= DateTime.Now.Date)
                this.lottoMasterBindingSource.Filter = String.Format("LottoRewardDate >= '{0:yyyy-MM-dd}' AND LottoRewardDate < '{1:yyyy-MM-dd}'", dateTimeInput1.Value, dateTimeInput1.Value.AddDays(1));
            else
                this.lottoItemBindingSource.RemoveFilter();
        }
        #endregion

        #region Print
        private void btPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (lottoMasterBindingSource.Count == 0) return;
                if (lottoMasterBindingSource.Position == -1) return;
                DataRowView rowView = (DataRowView)this.lottoMasterBindingSource.Current;
                if (rowView == null) return;
                string DocumentNumber = rowView["LottoDocumentNumber"].ToString();
                Report.ReportLottoMasterForm dlg = new Report.ReportLottoMasterForm("RL001", @"LottoDocumentNumber=""" + DocumentNumber+ @"""" );
                dlg.MdiParent = this.MdiParent;
                dlg.Show();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Events
        private void customerCodeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void dataGridViewX2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void dataGridViewX2_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                var datagrid = sender as DevComponents.DotNetBar.Controls.DataGridViewX;

                if (datagrid.SelectedRows.Count == 0) return;
                if (e.Button == MouseButtons.Right)
                {
                    int currentMouseOverRow = datagrid.HitTest(e.X, e.Y).RowIndex;
                    var rowselect = datagrid.SelectedRows[0];
                    if (currentMouseOverRow >= 0 && (rowselect.Index == currentMouseOverRow))
                    {
                        contextMenuStrip1.Show(datagrid, new Point(e.X, e.Y));

                        //  m.MenuItems.Add(new MenuItem(string.Format("Do something to row {0}", currentMouseOverRow.ToString())));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }

        private void btReload_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (this.textBoxX2.TextLength < 3) return;
            var lottonumber = this.textBoxX2.Text;
            try
            {
                int amount = 0;
                //var TempAmount = this.lottoItemTableAdapter.GetSumLottoAmount(lottonumber, this.dateTimeInput1.Value.Date);
                //if (TempAmount != null)
                {
                    DataRowView rowView = (DataRowView)this.lottoMasterBindingSource.Current;
                    int LottoID = (int)rowView["LottoID"];
                    var selectCase = string.Format("LottoID = {0} AND  LottoType ='{1}' AND LottoNumber = '{2}' AND PrintSequenceNumber = 0"
                    , LottoID, "", lottonumber);

                    var rowresult = this.ligorLottoDataSet.LottoItem.Select(selectCase);
                    if (rowresult == null || rowresult.Length == 0) return;
                    foreach (DataRow rowitem in rowresult)
                    {
                        amount += Convert.ToInt32(rowitem["LottoBalanceAmount"].ToString());
                    }
                    //amount = Convert.ToInt32(TempAmount);
                    RemoveLottoNumberDialog dlg = new RemoveLottoNumberDialog();
                    dlg.LottoAmount = amount;
                    dlg.LottoNumber = lottonumber;

                    if (dlg.ShowDialog() == DialogResult.Yes)
                    {
                        

                        if (dlg.LottoAmountEdited == 0)
                        {
                            for (int i = 0; i < rowresult.Length; i++)
                            {
                                rowresult[i].Delete();
                            }

                        }
                        else
                        {
                            if(rowresult.Length == 1)
                            {
                                rowresult[0]["LottoBalanceAmount"] = dlg.LottoAmountEdited;
                                rowresult[0]["LottoAmount"] = dlg.LottoAmountEdited;
                                rowresult[0]["UpdateUser"] = Environment.MachineName; 
                                rowresult[0]["UpdateWhen"] = DateTime.Now;
                            }
                            else
                            {
                                DataRow rowInternal = rowresult[0];
                                DataRow rowExternal = rowresult[1];
                                double AmountMax = 0;
                                double AmountExternal = 0;
                                double SumAmount = 0;
                                foreach (DataRow rowitem in rowresult)
                                {
                                    if (rowitem["GroupType"].ToString() == "0")
                                    {
                                        var result = rowitem["LottoBalanceAmount"].ToString();
                                        AmountMax = Convert.ToDouble(rowitem["LottoBalanceAmount"].ToString());
                                        rowInternal = rowitem;
                                    }
                                    else
                                    {
                                        AmountExternal = Convert.ToDouble(rowitem["LottoBalanceAmount"].ToString());
                                        rowExternal = rowitem;
                                    }
                                       
                                }
                                SumAmount = AmountMax + AmountExternal;
                                if(dlg.LottoAmountEdited  <=  AmountMax)
                                {
                                    rowExternal.Delete();
                                    Console.WriteLine("GroupType={0} Change", rowInternal["GroupType"].ToString());
                                    rowInternal["LottoAmount"] = dlg.LottoAmountEdited;
                                    rowInternal["LottoBalanceAmount"] = dlg.LottoAmountEdited;
                                    rowInternal["UpdateUser"] = Environment.MachineName;
                                    rowInternal["UpdateWhen"] = DateTime.Now;
                                }
                                else if(dlg.LottoAmountEdited > AmountMax)
                                {
                                    double NewAmount = dlg.LottoAmountEdited - AmountMax;
                                    Console.WriteLine("GroupType={0} Change", rowExternal["GroupType"].ToString());
                                    rowExternal["LottoAmount"] = NewAmount;
                                    rowExternal["LottoBalanceAmount"] = NewAmount;
                                    rowExternal["UpdateUser"] = Environment.MachineName;
                                    rowExternal["UpdateWhen"] = DateTime.Now;
                                }
                            }
                            // update
                           // row[0].Cells["dg_LottoAmount"].Value = dlg.LottoAmountEdited;
                        }
                        UpdateAll();
                    }
                }
                   
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void textBoxX2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btEdit_Click(this, new EventArgs());
            }
        }

        private void textBoxX2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
            }
        }
        #endregion


    }
}
