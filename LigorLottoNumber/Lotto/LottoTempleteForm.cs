﻿using LigorLottoNumber.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Lotto
{
    public partial class LottoTempleteForm : Base.ChildForm
    {

        private bool _isStartup = true;
        private TableEntity table = new TableEntity();

        #region Constructor
        public LottoTempleteForm()
        {
            InitializeComponent();
        }
        #endregion

        #region Reload
        private void Reload()
        {
            try
            {
                this.panelEx1.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                if (_isStartup)
                {
                    comboBoxEx1.Items.Clear();
                    for (int i = 0; i < 10; i++)
                    {
                        comboBoxEx1.Items.Add(string.Format("หมวด {0}XX",i));
                    }

                   
                    _isStartup = false;
                }

                table.Source = TableEntity.Database.LigorLotto;
                string str = "SELECT DISTINCT TitleName FROM LottoTemplate ORDER BY TitleName ASC";
                if(table.SQLQuery(str))
                {
                    comboBoxEx2.DisplayMember = "TitleName";
                    comboBoxEx2.ValueMember = "TitleName";
                    comboBoxEx2.DataSource = table.Records;
                }


                // Load LottoTemplate
                this.lottoTemplateTableAdapter.Fill(this.ligorLottoDataSet.LottoTemplate);

#if DEBUG
                //if (this.lottoTemplateBindingSource.Count < 1000)
                //{
                //    InitialLottoTemplete();
                //}

                //InitialLottoTemplete("หมวด C");
                //InitialLottoTemplete("หมวด D");
                //InitialLottoTemplete("หมวด E");
#endif
                if (this.lottoTemplateBindingSource.Sort == null)
                {
                    this.lottoTemplateBindingSource.Sort = "LottoNumber ASC";
                }



                comboBoxEx1.SelectedIndex = 0;
                radioButton1.Checked = true;
               
                Console.Beep(1000, 500);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,this.Name,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                this.panelEx1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void LottoTempleteForm_Load(object sender, EventArgs e)
        {
            Reload();
        }

        private void InitialLottoTemplete(string titlename = "")
        {
            Console.WriteLine("กำลังเพิ่ม หมวด "+titlename);
            for (int i = 0; i < 1000; i++)
            {
                string number = i.ToString();
                while (number.Length < 3) number = "0" + number;
                this.lottoTemplateTableAdapter.Insert(number, null, null, titlename);
            }
        }

#endregion

        #region Relink
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RelinkGroupLottoNumber();
        }

        private void RelinkGroupLottoNumber()
        {
            comboBoxEx1.Enabled = false;
            string titlename = comboBoxEx2.SelectedValue != null ? string.Format(" AND TitleName = '{0}'",comboBoxEx2.SelectedValue.ToString()) : "";
            if (radioButton1.Checked)
            {
                if (titlename.Length > 0)
                    this.lottoTemplateBindingSource.Filter = titlename.Replace("AND", "").Trim();
                else
                    this.lottoTemplateBindingSource.RemoveFilter();
            }
            else
            {
                comboBoxEx1.Enabled = true;
                this.lottoTemplateBindingSource.Filter = string.Format("LottoNumber like '{0}*' {1}", comboBoxEx1.SelectedIndex, titlename);
            }
            
        }
        private void comboBoxEx1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RelinkGroupLottoNumber();
        }
        private void comboBoxEx2_SelectedValueChanged(object sender, EventArgs e)
        {
            RelinkGroupLottoNumber();
        }
        #endregion

        #region UpdateAll
        private void UpdateAll()
                {
                    try
                    {
                        this.panelEx1.Enabled = false;
                        this.Cursor = Cursors.WaitCursor;
                        this.lottoTemplateBindingSource.EndEdit();

                        this.lottoTemplateTableAdapter.Update(ligorLottoDataSet.LottoTemplate);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        this.panelEx1.Enabled = true;
                        this.Cursor = Cursors.Default;
                    }
                }
                private void btSave_Click(object sender, EventArgs e)
                {
                    UpdateAll();
                }
        #endregion

        #region Clear MaimumAmount
                private void ClearMaximumAmount()
                {
                    try
                    {
                        this.panelEx1.Enabled = false;
                        this.Cursor = Cursors.WaitCursor;

                        foreach (DataRow row in ligorLottoDataSet.LottoTemplate.Rows)
                        {
                            if (row["MaximumAmount"] != DBNull.Value || row["Remark"] != DBNull.Value)
                            {
                                row["MaximumAmount"] = DBNull.Value;
                                row["Remark"] = DBNull.Value;
                            }
                        }

                        UpdateAll();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        this.panelEx1.Enabled = true;
                        this.Cursor = Cursors.Default;
                    }
                }
        private void btClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการล้างราคาเลขอั้นทั้งหมดหรือไม่?", "ล้างราคาเลขอั้นทั้งหมด", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dataGridViewX1.Rows)
                {
                    if(row.Cells["dg_MaximumAmount"].Value != DBNull.Value)
                        row.Cells["dg_MaximumAmount"].Value = DBNull.Value;
                }
                UpdateAll();
                //ClearMaximumAmount();
            }
        }
        #endregion

        private void buttonX1_Click(object sender, EventArgs e)
        {
            double valueAmount = doubleInput1.Value;
            if (MessageBox.Show(string.Format("คุณต้องการกำหนดจำนวนเงินมากที่สุด {0} ให้ทุกตัวเลขหรือไม่?",valueAmount.ToString("N0")), "อั้นทุกตัวเลข", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                
                foreach (DataGridViewRow row in dataGridViewX1.Rows)
                {
                    row.Cells["dg_MaximumAmount"].Value = valueAmount;
                }
                UpdateAll();
            }
        }

        private void doubleInput1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                buttonX1_Click(this, new EventArgs());
        }

        private void comboBoxEx2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}
