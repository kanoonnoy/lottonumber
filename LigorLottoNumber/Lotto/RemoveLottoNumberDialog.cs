﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Lotto
{
    public partial class RemoveLottoNumberDialog : Form
    {
        public string LottoNumber { get; set; }
        public int LottoAmount { get; set; }
        public int LottoAmountEdited { get; set; }
        public RemoveLottoNumberDialog()
        {
            InitializeComponent();
        }
        private void RemoveLottoNumberDialog_Load(object sender, EventArgs e)
        {
            textBoxX1.Text = LottoNumber;
            labelOriginal.Text = LottoAmount.ToString("N0");
            integerInput1.MaxValue = LottoAmount;
            integerInput1.Value = LottoAmount;
            integerInput1.MinValue = 0;
        }
        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                string text = string.Format("คุณต้องการแก้ไขจำนวนเงินของตัวเลข '{0}' จาก {1:N0} เป็น {2:N0} หรือไม่?", LottoNumber, LottoAmount, LottoAmountEdited);
                if (MessageBox.Show(text, "แก้ไขจำนวนเงิน", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    LottoAmountEdited = LottoAmount - integerInput1.Value;
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        private void integerInput1_ValueChanged(object sender, EventArgs e)
        {
            LottoAmountEdited = LottoAmount - integerInput1.Value;
            labelEdited.Text = LottoAmountEdited.ToString("N0");
        }

        private void integerInput1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void integerInput1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                buttonX1_Click(this, new EventArgs());
            }
        }
    }
}
