﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Globalization;
using System.Diagnostics;

namespace LigorLottoNumber.Report
{
    public partial class ReportViewer : UserControl
    {
        #region Varible
        private string _DefaultPath = "D:\\Report";
        private string _DefaultDBServer = "D:\\DataBase\\LigorLotto.mdb";
        private string _reportNumber = "";
        private string _ParameterFildText = string.Empty;
        public string ReportNumber { get { return _reportNumber; } set { _reportNumber = value; } }
        private ReportDocument crystalDocument = new ReportDocument();
        public string ParameterFildText { set { _ParameterFildText = value; } }
        #endregion
        public ReportViewer()
        {
            InitializeComponent();
            initialtoolbar();
            _DefaultPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath)+"\\Report";
            _DefaultDBServer   = System.IO.Path.GetDirectoryName(Application.ExecutablePath)+"\\LigorLotto.mdb";


        }
        private void initialtoolbar()
        {
            this.crystalViewer.ShowPrintButton = false;
            this.crystalViewer.ShowRefreshButton = false;
            this.crystalViewer.ShowExportButton = false;
            this.crystalViewer.ShowCloseButton = false;
            this.crystalViewer.ShowLogo = false;
            this.crystalViewer.ShowCopyButton = false;
            this.crystalViewer.ShowParameterPanelButton = false;
            this.crystalViewer.ShowGroupTreeButton = false;
        }
        public void Reload()
        {
            try
            {
                this.crystalViewer.Visible = false;
                string filefullPath = string.Format("{0}\\{1}.rpt", _DefaultPath, _reportNumber);

                if (!File.Exists(filefullPath)) return;
                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;
                crystalDocument = new ReportDocument();
                crystalDocument.Load(filefullPath);
                crConnectionInfo.ServerName = _DefaultDBServer;
                CrTables = crystalDocument.Database.Tables;

                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }

                this.crystalDocument.SetDatabaseLogon("", "", _DefaultDBServer, "");
                this.crystalViewer.ShowGroupTreeButton = false;

                if (_ParameterFildText.Length > 0) 
                    PassReportParameters(_ParameterFildText);

                this.crystalViewer.ReportSource = crystalDocument;
                this.crystalViewer.ShowGroupTree();
                this.crystalViewer.Refresh();
            
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.crystalViewer.Visible = true ;
            }
        }
        private void PassReportParameters()
        {

            ParameterFieldDefinition parameterFieldDefinition;
            ParameterFields parameterFields;
            ParameterField parameterField;
            ParameterValues parameterValues;
            ParameterDiscreteValue parameterDiscreteValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime dateParameter;
            string stringParameter;
            int intParameter;
            string paramFieldName, paramFieldValues;
            string[] paramFieldValue;

            if (crystalDocument.ParameterFields.Count > 0)
            {
                if (_ParameterFildText != null)
                {
                    crystalViewer.ReuseParameterValuesOnRefresh = true;
                    parameterFields = crystalViewer.ParameterFieldInfo;

                    foreach (string parameter in _ParameterFildText.Split(';'))
                    {
                        try
                        {
                            string[] parameterfield = parameter.Split('=');

                            if (parameterfield.Length == 2)
                            {
                                paramFieldName = parameterfield[0];
                                paramFieldValues = parameterfield[1];


                                parameterFieldDefinition = crystalDocument.DataDefinition.ParameterFields[paramFieldName];

                                if (parameterFieldDefinition.DiscreteOrRangeKind == DiscreteOrRangeKind.DiscreteValue)
                                {
                                    parameterField = parameterFields[paramFieldName];
                                    parameterValues = parameterField.CurrentValues;
                                    parameterValues.Clear();

                                    paramFieldValue = paramFieldValues.Split(',');
                                    foreach (string paramValue in paramFieldValue)
                                    {
                                        //DiscreteValue
                                        parameterDiscreteValue = new ParameterDiscreteValue();
                                        if (paramValue.Length == 12 && paramValue.Substring(0, 1) == "#" && paramValue.Substring(5, 1) == "-" && paramValue.Substring(8, 1) == "-" && paramValue.Substring(11, 1) == "#")
                                        {
                                            //IsDate
                                            dateParameter = new DateTime();
                                            dateParameter = DateTime.ParseExact(paramValue.Substring(1, 10), "yyyy-MM-dd", provider);
                                            parameterDiscreteValue.Value = dateParameter;
                                        }
                                        else if ((paramValue.Substring(0, 1) == "\"" && paramValue.Substring(paramValue.Length - 1, 1) == "\"") || (paramValue.Substring(0, 1) == "'" && paramValue.Substring(paramValue.Length - 1, 1) == "'"))
                                        {
                                            //IsString
                                            stringParameter = paramValue.Substring(1, paramValue.Length - 2);
                                            parameterDiscreteValue.Value = stringParameter;
                                        }
                                        //else if (!parameter.IsAllDigits())
                                        //{
                                        //    //IsString Fixed
                                        //    stringParameter = paramValue;
                                        //    parameterDiscreteValue.Value = stringParameter;
                                        //}
                                        else
                                        {
                                            //IsInteger
                                            intParameter = int.Parse(paramValue);
                                            parameterDiscreteValue.Value = intParameter;
                                        }
                                        parameterValues.Add(parameterDiscreteValue);
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            Debug.Print("Error Passing Parameter: " + ex.Message);
                        }
                    }
                }
            }
        }
        private void PassReportParametersDefault()
        {
            ParameterValues parameterValues;

            ParameterDiscreteValue parameterDiscreteValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            //DateTime dateParameter;
            string stringParameter;
            //int intParameter;

            if (crystalViewer.ParameterFieldInfo.Count > 0)
            {

                try
                {
                    foreach (ParameterField parameterField in crystalViewer.ParameterFieldInfo)
                    {


                        if (parameterField.ParameterValueType == ParameterValueKind.StringParameter)
                        {
                            parameterValues = parameterField.CurrentValues;


                            parameterDiscreteValue = new ParameterDiscreteValue();
                            stringParameter = "*";
                            parameterDiscreteValue.Value = stringParameter;
                            parameterValues.Clear();
                            parameterValues.Add(parameterDiscreteValue);
                        }

                    }


                    crystalViewer.ReuseParameterValuesOnRefresh = true;

                }
                catch (Exception ex)
                {
                    Debug.Print("Error Passing Parameter: " + ex.Message);
                }
            }


        }

        private void PassReportParameters(string ParameterFieldsText)
        {

            CrystalDecisions.Shared.ParameterField parameterField;

            ParameterValues parameterValues;
            ParameterDiscreteValue parameterDiscreteValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime dateParameter;
            string stringParameter;
            int intParameter;

            string paramFieldName, paramFieldValues;
            string[] paramFieldValue;



            foreach (string parameter in ParameterFieldsText.Split(';'))
            {
                try
                {
                    string[] parameterfield = parameter.Split('=');

                    if (parameterfield.Length == 2)
                    {
                        paramFieldName = parameterfield[0];
                        paramFieldValues = parameterfield[1];


                        parameterField = crystalDocument.ParameterFields[paramFieldName];

                        if (parameterField.DiscreteOrRangeKind == DiscreteOrRangeKind.DiscreteValue)
                        {

                            parameterValues = parameterField.CurrentValues;
                            parameterValues.Clear();

                            paramFieldValue = paramFieldValues.Split(',');
                            foreach (string paramValue in paramFieldValue)
                            {
                                //DiscreteValue
                                parameterDiscreteValue = new ParameterDiscreteValue();
                                if (paramValue.Length == 12 && paramValue.Substring(0, 1) == "#" && paramValue.Substring(5, 1) == "-" && paramValue.Substring(8, 1) == "-" && paramValue.Substring(11, 1) == "#")
                                {
                                    //IsDate
                                    dateParameter = new DateTime();
                                    dateParameter = DateTime.ParseExact(paramValue.Substring(1, 10), "yyyy-MM-dd", provider);
                                    parameterDiscreteValue.Value = dateParameter;
                                }
                                else if (paramValue.Substring(0, 1) == "\"" && paramValue.Substring(paramValue.Length - 1, 1) == "\"")
                                {
                                    //IsString
                                    stringParameter = paramValue.Substring(1, paramValue.Length - 2);
                                    parameterDiscreteValue.Value = stringParameter;
                                }
                                else
                                {
                                    //IsInteger
                                    intParameter = int.Parse(paramValue);
                                    parameterDiscreteValue.Value = intParameter;
                                }
                                parameterValues.Add(parameterDiscreteValue);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Debug.Print("Error Passing Parameter: " + ex.Message);

                }

            }
        }

        private void reportDocument1_InitReport(object sender, EventArgs e)
        {

        }

        private void crystalViewer_ReportRefresh(object source, CrystalDecisions.Windows.Forms.ViewerEventArgs e)
        {

        }


        public void PrintReport()
        {
            crystalViewer.PrintReport();
        }
    }
}
