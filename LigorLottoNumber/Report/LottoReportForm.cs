﻿using LigorLottoNumber.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Report
{
    public partial class LottoReportForm : Base.ChildForm
    {
        private LigorLottoDataSetTableAdapters.LottoItemTableAdapter taitem = new LigorLottoDataSetTableAdapters.LottoItemTableAdapter();
        private string _ReportNumber = string.Empty; 
        public string ReportNumber { get { return _ReportNumber; } set { _ReportNumber = value; } }
        private Base.TableEntity table = new Base.TableEntity();
        public LottoReportForm()
        {
            
            InitializeComponent();

            PrintComboBoxEx1.Visible = false;
            labelX4.Visible = false;
        }

        #region Reload
        public void Reload()
        {
            try
            {
                _ReportNumber =base.FormCustomization ;
                if (_ReportNumber.Equals("RL005"))
                {
                    PrintComboBoxEx1.Visible = true;
                    labelX4.Visible = true;
                }

                this.panelEx1.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                DateTime Preiod1 = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);
                DateTime Preiod2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
                if (DateTime.Now.Day > 16)
                    IssueWhenDatePicker.Value = Preiod1;
                else
                    IssueWhenDatePicker.Value = Preiod2;


                table.Source = TableEntity.Database.LigorLotto;
                string str = "SELECT DISTINCT TitleName FROM LottoTemplate ORDER BY TitleName ASC";
                if (table.SQLQuery(str))
                {
                    comboBoxEx2.DisplayMember = "TitleName";
                    comboBoxEx2.ValueMember = "TitleName";
                    comboBoxEx2.DataSource = table.Records;
                }

                InitialPrint();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.panelEx1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void LottoReportForm_Shown(object sender, EventArgs e)
        {
            Reload();
        }
        #endregion

        #region Report
        private void InitialPrint()
        {

            table.Source = TableEntity.Database.LigorLotto;
            var getprint = "SELECT DISTINCT PrintSequenceNumber FROM TABLE_LottoRewardByUpdateAndPrint ORDER BY PrintSequenceNumber";
            if (table.SQLQuery(getprint))
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Value", typeof(int));
                dt.Columns.Add("Member");
                //dt.Columns.Add("");
                foreach (DataRow row in table.Records.Rows)
                {
                    int value = row["PrintSequenceNumber"] == DBNull.Value ? 0 : (int)row["PrintSequenceNumber"];
                    string member = value == 0 ? "ยังไม่ได้พิมพ์" : string.Format("พิมพ์ครั้งที่ {0}", row["PrintSequenceNumber"].ToString());
                    //data.Add(new DataPrint() { Value = value, Member = member });
                    dt.Rows.Add(value, member);
                }

                PrintComboBoxEx1.DisplayMember = "Member";
                PrintComboBoxEx1.ValueMember = "Value";
                PrintComboBoxEx1.DataSource = dt;

                
            }
        }
        private void RelinkReport()
        {
            try
            {
                if (PrintComboBoxEx1.SelectedValue == null)
                {
                    MessageBox.Show("ยังไม่มีข้อมูล กรุณากรอกข้อมูลก่อนพิมพ์ค่ะ","พิมพ์เอกสาร",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;

                int GroupNumber = 1;
                if (radioButton2.Checked) GroupNumber = 2;
                string parameter =
                      @"GroupNumber=" + GroupNumber + @"" + ";"
                    + @"LottoPeriod=""" + IssueWhenDatePicker.Value.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("th-th")) + @"""" + ";"
                    + @"TitleName=""" + comboBoxEx2.SelectedValue.ToString() + @"""" + ";"
                    ;
                if(_ReportNumber.Equals("RL004"))
                {
                    parameter += @"PrintSequenceNumber=" + PrintComboBoxEx1.SelectedValue.ToString() + @"";
                }
                if(_ReportNumber.Equals("RL005"))
                {
                    string GroupType = radioButton2.Checked ? "1" : "0";
                    parameter = @"GroupNumber=""" + GroupType + @"""" + ";"
                    + @"LottoPeriod=""" + IssueWhenDatePicker.Value.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("th-th")) + @"""" + ";"
                    + @"PrintSequenceNumber=" + PrintComboBoxEx1.SelectedValue.ToString() + @"";
                    ;
                }
                this.reportViewer1.ReportNumber = _ReportNumber;
                this.reportViewer1.ParameterFildText = parameter;
                this.reportViewer1.Reload();
                //this.reportViewer1.Refresh();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,this.Name,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void buttonX2_Click(object sender, EventArgs e)
        {
            RelinkReport();
        }

        #endregion

        #region Relink
        private void Relink()
        {
            try
            {
                //double CountSet = 0;
                //string str = string.Format("SELECT MAX(CountSet) AS CountSet  FROM TABLE_LottoReward WHERE LottoRewardDate = #{0}#", IssueWhenDatePicker.Value.Date.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")));
                //table.Source = Base.TableEntity.Database.LigorLotto;
                //if (table.SQLQuery(str))
                //{
                //    if (table.Records.Rows.Count > 0)
                //    {
                //        string coutset = table.Records.Rows[0]["CountSet"].ToString();
                //        if (double.TryParse(coutset, out CountSet))
                //        {
                //            CountSet = Math.Ceiling(CountSet);
                //        }
                //    }
                //}
                //if (CountSet == 0) CountSet = 1;
                //for (int i = 1; i <= (int)CountSet; i++)
                //{
                //    comboBoxEx1.Items.Add(i);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }
        }
        private void buttonX1_Click(object sender, EventArgs e)
        {
            Relink();
        }
        private void IssueWhenDatePicker_ValueChanged(object sender, EventArgs e)
        {
            Relink();
        }
        #endregion

        #region print
        private void btprint_Click(object sender, EventArgs e)
        {
            try
            {
                this.btprint.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                this.reportViewer1.PrintReport();

                if(PrintComboBoxEx1.SelectedValue.ToString() == "0")
                {

                    int PrintSequenceNumber = 0;

                    var checklast = "SELECT DISTINCT PrintSequenceNumber FROM TABLE_LottoRewardByUpdateAndPrint ORDER BY PrintSequenceNumber DESC";
                    table.Source = TableEntity.Database.LigorLotto;
                    if(table.SQLQuery(checklast))
                    {
                        if(table.Records.Rows.Count > 0 )
                        {
                            PrintSequenceNumber = Convert.ToInt32(table.Records.Rows[0]["PrintSequenceNumber"].ToString());
                            if (PrintSequenceNumber == 0) PrintSequenceNumber = 1;
                            else PrintSequenceNumber++;
                             
                        }
                    }

                    var result = string.Format("SELECT LottoItem.* FROM LottoMaster "
                    + " LEFT JOIN LottoItem ON LottoMaster.LottoID = LottoItem.LottoID"
                    + " WHERE DATEVALUE(LottoMaster.LottoRewardDate) = #{0}# AND LottoItem.PrintSequenceNumber = 0", IssueWhenDatePicker.Value.ToString("yyyy-MM-dd",new System.Globalization.CultureInfo("en-US")))
                    ;

                    table.Source = TableEntity.Database.LigorLotto;
                    if (table.SQLQuery(result))
                    {
                        Console.WriteLine("update {0} rows", table.Records.Rows.Count);
                        foreach (DataRow row in table.Records.Rows)
                        {
                            row["PrintSequenceNumber"] = PrintSequenceNumber;
                            
                            // update 
                        }


                        int updatecount = taitem.Update(table.Records.Select());

                        Console.WriteLine("Update {0} rows",updatecount);

                        if(updatecount > 0 )
                        {
                            InitialPrint();
                            if (PrintComboBoxEx1.Items.Count > 0)
                            {
                                PrintComboBoxEx1.SelectedIndex = PrintComboBoxEx1.Items.Count - 1;
                            }
                            // reload
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            finally
            {
                this.btprint.Enabled = true;
                this.Cursor = Cursors.Default;
            }
            // save


        }
        
        private void labelX4_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            InitialPrint();
            if (PrintComboBoxEx1.Items.Count > 0)
            {
                PrintComboBoxEx1.SelectedIndex = PrintComboBoxEx1.Items.Count - 1;
            }
        }
        #endregion

    }
}
