﻿namespace LigorLottoNumber.Report
{
    partial class LottoReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.btprint = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.PrintComboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.comboBoxEx2 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.IssueWhenDatePicker = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.reportViewer1 = new LigorLottoNumber.Report.ReportViewer();
            this.panelEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IssueWhenDatePicker)).BeginInit();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.btprint);
            this.panelEx1.Controls.Add(this.labelX4);
            this.panelEx1.Controls.Add(this.PrintComboBoxEx1);
            this.panelEx1.Controls.Add(this.labelX3);
            this.panelEx1.Controls.Add(this.comboBoxEx2);
            this.panelEx1.Controls.Add(this.radioButton2);
            this.panelEx1.Controls.Add(this.radioButton1);
            this.panelEx1.Controls.Add(this.buttonX2);
            this.panelEx1.Controls.Add(this.labelX2);
            this.panelEx1.Controls.Add(this.labelX1);
            this.panelEx1.Controls.Add(this.IssueWhenDatePicker);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Margin = new System.Windows.Forms.Padding(4);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(882, 75);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            // 
            // btprint
            // 
            this.btprint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btprint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btprint.Location = new System.Drawing.Point(754, 12);
            this.btprint.Name = "btprint";
            this.btprint.Size = new System.Drawing.Size(95, 23);
            this.btprint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btprint.TabIndex = 16;
            this.btprint.Text = "พิมพ์";
            this.btprint.Click += new System.EventHandler(this.btprint_Click);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.Location = new System.Drawing.Point(451, 12);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(56, 23);
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "สถานะ";
            this.labelX4.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelX4_MouseDoubleClick);
            // 
            // PrintComboBoxEx1
            // 
            this.PrintComboBoxEx1.DisplayMember = "Text";
            this.PrintComboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.PrintComboBoxEx1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PrintComboBoxEx1.FormattingEnabled = true;
            this.PrintComboBoxEx1.ItemHeight = 18;
            this.PrintComboBoxEx1.Location = new System.Drawing.Point(513, 11);
            this.PrintComboBoxEx1.Name = "PrintComboBoxEx1";
            this.PrintComboBoxEx1.Size = new System.Drawing.Size(134, 24);
            this.PrintComboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PrintComboBoxEx1.TabIndex = 14;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.Location = new System.Drawing.Point(290, 12);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(50, 23);
            this.labelX3.TabIndex = 13;
            this.labelX3.Text = "ชื่อหมวด";
            // 
            // comboBoxEx2
            // 
            this.comboBoxEx2.DisplayMember = "Text";
            this.comboBoxEx2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEx2.FormattingEnabled = true;
            this.comboBoxEx2.ItemHeight = 18;
            this.comboBoxEx2.Location = new System.Drawing.Point(346, 11);
            this.comboBoxEx2.Name = "comboBoxEx2";
            this.comboBoxEx2.Size = new System.Drawing.Size(99, 24);
            this.comboBoxEx2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx2.TabIndex = 12;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.ForeColor = System.Drawing.Color.Navy;
            this.radioButton2.Location = new System.Drawing.Point(247, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(146, 22);
            this.radioButton2.TabIndex = 7;
            this.radioButton2.Tag = "2";
            this.radioButton2.Text = "2 รอง(สำหรับส่งออก)";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.ForeColor = System.Drawing.Color.Navy;
            this.radioButton1.Location = new System.Drawing.Point(72, 41);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(169, 22);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "1";
            this.radioButton1.Text = "1 หลัก(สำหรับเจ้ามือหวย)";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(653, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(95, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 5;
            this.buttonX2.Text = "แสดงข้อมูล";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.Location = new System.Drawing.Point(34, 41);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(32, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "ชุดที่";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.Location = new System.Drawing.Point(36, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(54, 23);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "งวดวันที่";
            // 
            // IssueWhenDatePicker
            // 
            // 
            // 
            // 
            this.IssueWhenDatePicker.BackgroundStyle.Class = "DateTimeInputBackground";
            this.IssueWhenDatePicker.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.IssueWhenDatePicker.ButtonDropDown.Visible = true;
            this.IssueWhenDatePicker.Format = DevComponents.Editors.eDateTimePickerFormat.Long;
            this.IssueWhenDatePicker.Location = new System.Drawing.Point(96, 11);
            // 
            // 
            // 
            this.IssueWhenDatePicker.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.IssueWhenDatePicker.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.IssueWhenDatePicker.MonthCalendar.BackgroundStyle.Class = "";
            this.IssueWhenDatePicker.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.IssueWhenDatePicker.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.IssueWhenDatePicker.MonthCalendar.DisplayMonth = new System.DateTime(2016, 8, 1, 0, 0, 0, 0);
            this.IssueWhenDatePicker.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.IssueWhenDatePicker.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.IssueWhenDatePicker.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.IssueWhenDatePicker.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.IssueWhenDatePicker.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.IssueWhenDatePicker.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.IssueWhenDatePicker.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.IssueWhenDatePicker.MonthCalendar.TodayButtonVisible = true;
            this.IssueWhenDatePicker.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.IssueWhenDatePicker.Name = "IssueWhenDatePicker";
            this.IssueWhenDatePicker.Size = new System.Drawing.Size(188, 24);
            this.IssueWhenDatePicker.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.IssueWhenDatePicker.TabIndex = 0;
            this.IssueWhenDatePicker.ValueChanged += new System.EventHandler(this.IssueWhenDatePicker_ValueChanged);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.reportViewer1.Location = new System.Drawing.Point(0, 75);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(4);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ReportNumber = "";
            this.reportViewer1.Size = new System.Drawing.Size(882, 413);
            this.reportViewer1.TabIndex = 1;
            // 
            // LottoReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 488);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.panelEx1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "LottoReportForm";
            this.Text = "รายงานสรุปหวยประจำงวดแยกตามชุด";
            this.Shown += new System.EventHandler(this.LottoReportForm_Shown);
            this.panelEx1.ResumeLayout(false);
            this.panelEx1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IssueWhenDatePicker)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput IssueWhenDatePicker;
        private ReportViewer reportViewer1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx PrintComboBoxEx1;
        private DevComponents.DotNetBar.ButtonX btprint;
    }
}