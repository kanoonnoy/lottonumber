﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Report
{
    public partial class ReportLottoMasterForm : Base.ChildForm
    {
        private string _ReportNumber = string.Empty;
        private string _ParameterFildText = string.Empty;
        public string BinddingReportNumber {set { _ReportNumber = value; } }
        public string ParameterFildText { set { _ParameterFildText = value; } }
        public ReportLottoMasterForm(string ReportNumber,string parameterfildtext)
        {
            InitializeComponent();
            _ReportNumber = ReportNumber;
            _ParameterFildText = parameterfildtext;
        }

        private void ReportLottoMasterForm_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if(_ReportNumber.Length > 0)
            {
                reportViewer1.ReportNumber = _ReportNumber;
                reportViewer1.ParameterFildText = _ParameterFildText;
                reportViewer1.Reload();
            }
        }
    }
}
