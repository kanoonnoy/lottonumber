﻿namespace LigorLottoNumber.Report
{
    partial class ReportViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // crystalViewer
            // 
            this.crystalViewer.ActiveViewIndex = -1;
            this.crystalViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalViewer.Location = new System.Drawing.Point(0, 0);
            this.crystalViewer.Margin = new System.Windows.Forms.Padding(4);
            this.crystalViewer.Name = "crystalViewer";
            this.crystalViewer.Size = new System.Drawing.Size(531, 458);
            this.crystalViewer.TabIndex = 0;
            this.crystalViewer.ToolPanelWidth = 150;
            this.crystalViewer.ReportRefresh += new CrystalDecisions.Windows.Forms.RefreshEventHandler(this.crystalViewer_ReportRefresh);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.Location = new System.Drawing.Point(4, 4);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(386, 32);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "กำลังเตรียมข้อมูลกรุณารอสักครู่...";
            // 
            // ReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.crystalViewer);
            this.Controls.Add(this.labelX1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReportViewer";
            this.Size = new System.Drawing.Size(531, 458);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalViewer;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}
