﻿namespace LigorLottoNumber
{
    partial class MainParentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("เลขสามตัวบน", 12, 12);
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("บันทึกเลขหวย", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("ตั้งค่าเลขอั้น", 6, 6);
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("ตั้งค่าหวย", 29, 29, new System.Windows.Forms.TreeNode[] {
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("บิลหวยแยกตามลูกค้า", 15, 15);
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("สรุปบิลหวยทั้งหมดประจำงวด", 16, 16);
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("สรุปบิลหวยทั้งหมดแยกตามชุด", 32, 32);
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("สรุปบิลหวยทั้งหมดแยกตามชุดเรียงตามวันที่บันทึก", 34, 34);
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("รายงาน", 26, 26, new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainParentForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.จดการฐานขอมลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ลางขอมลลกคาทงหมดToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ตงรหสผานToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.navigationPane1 = new DevComponents.DotNetBar.NavigationPane();
            this.navigationPanePanel1 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.lottoMasterTableAdapter1 = new LigorLottoNumber.LigorLottoDataSetTableAdapters.LottoMasterTableAdapter();
            this.lottoItemTableAdapter1 = new LigorLottoNumber.LigorLottoDataSetTableAdapters.LottoItemTableAdapter();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.navigationPane1.SuspendLayout();
            this.navigationPanePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.จดการฐานขอมลToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(843, 28);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // จดการฐานขอมลToolStripMenuItem
            // 
            this.จดการฐานขอมลToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem,
            this.ลางขอมลลกคาทงหมดToolStripMenuItem});
            this.จดการฐานขอมลToolStripMenuItem.Name = "จดการฐานขอมลToolStripMenuItem";
            this.จดการฐานขอมลToolStripMenuItem.Size = new System.Drawing.Size(116, 24);
            this.จดการฐานขอมลToolStripMenuItem.Text = "จัดการฐานข้อมูล";
            // 
            // เรมงวดใหมลางขอมลทงหมดToolStripMenuItem
            // 
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem.Name = "เรมงวดใหมลางขอมลทงหมดToolStripMenuItem";
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem.Size = new System.Drawing.Size(253, 26);
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem.Text = "เริ่มงวดใหม่ (ล้างข้อมูลทุกงวด)";
            this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem.Click += new System.EventHandler(this.เรมงวดใหมลางขอมลทงหมดToolStripMenuItem_Click);
            // 
            // ลางขอมลลกคาทงหมดToolStripMenuItem
            // 
            this.ลางขอมลลกคาทงหมดToolStripMenuItem.Name = "ลางขอมลลกคาทงหมดToolStripMenuItem";
            this.ลางขอมลลกคาทงหมดToolStripMenuItem.Size = new System.Drawing.Size(253, 26);
            this.ลางขอมลลกคาทงหมดToolStripMenuItem.Text = "ล้างข้อมูลลูกค้าทั้งหมด";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ตงรหสผานToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem1.Text = "ตั้งค่าพื้นฐาน";
            // 
            // ตงรหสผานToolStripMenuItem
            // 
            this.ตงรหสผานToolStripMenuItem.Name = "ตงรหสผานToolStripMenuItem";
            this.ตงรหสผานToolStripMenuItem.Size = new System.Drawing.Size(147, 26);
            this.ตงรหสผานToolStripMenuItem.Text = "ตั้งรหัสผ่าน";
            this.ตงรหสผานToolStripMenuItem.Click += new System.EventHandler(this.ตงรหสผานToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 533);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(843, 25);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(49, 20);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // navigationPane1
            // 
            this.navigationPane1.CanCollapse = true;
            this.navigationPane1.ConfigureAddRemoveVisible = false;
            this.navigationPane1.ConfigureItemVisible = false;
            this.navigationPane1.ConfigureNavOptionsVisible = false;
            this.navigationPane1.Controls.Add(this.navigationPanePanel1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navigationPane1.ItemPaddingBottom = 2;
            this.navigationPane1.ItemPaddingTop = 2;
            this.navigationPane1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1});
            this.navigationPane1.Location = new System.Drawing.Point(0, 28);
            this.navigationPane1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.NavigationBarHeight = 33;
            this.navigationPane1.Padding = new System.Windows.Forms.Padding(1);
            this.navigationPane1.Size = new System.Drawing.Size(227, 505);
            this.navigationPane1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TabIndex = 4;
            // 
            // 
            // 
            this.navigationPane1.TitlePanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TitlePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigationPane1.TitlePanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationPane1.TitlePanel.Location = new System.Drawing.Point(1, 1);
            this.navigationPane1.TitlePanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPane1.TitlePanel.Name = "panelTitle";
            this.navigationPane1.TitlePanel.Size = new System.Drawing.Size(225, 30);
            this.navigationPane1.TitlePanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.navigationPane1.TitlePanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.navigationPane1.TitlePanel.Style.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.navigationPane1.TitlePanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPane1.TitlePanel.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.navigationPane1.TitlePanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.navigationPane1.TitlePanel.Style.GradientAngle = 90;
            this.navigationPane1.TitlePanel.Style.MarginLeft = 4;
            this.navigationPane1.TitlePanel.TabIndex = 0;
            this.navigationPane1.TitlePanel.Text = "เลขรางวัล";
            // 
            // navigationPanePanel1
            // 
            this.navigationPanePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel1.Controls.Add(this.treeView1);
            this.navigationPanePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel1.Location = new System.Drawing.Point(1, 31);
            this.navigationPanePanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPanePanel1.Name = "navigationPanePanel1";
            this.navigationPanePanel1.ParentItem = this.buttonItem1;
            this.navigationPanePanel1.Size = new System.Drawing.Size(225, 440);
            this.navigationPanePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel1.Style.GradientAngle = 90;
            this.navigationPanePanel1.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.treeView1.Name = "treeView1";
            treeNode1.ImageIndex = 12;
            treeNode1.Name = "Node1";
            treeNode1.SelectedImageIndex = 12;
            treeNode1.Tag = "LigorLottoNumber.Lotto.LottoCreatorForm";
            treeNode1.Text = "เลขสามตัวบน";
            treeNode2.Name = "Node0";
            treeNode2.Text = "บันทึกเลขหวย";
            treeNode3.ImageIndex = 6;
            treeNode3.Name = "Node1";
            treeNode3.SelectedImageIndex = 6;
            treeNode3.Tag = "LigorLottoNumber.Lotto.LottoTempleteForm";
            treeNode3.Text = "ตั้งค่าเลขอั้น";
            treeNode4.ImageIndex = 29;
            treeNode4.Name = "Node2";
            treeNode4.SelectedImageIndex = 29;
            treeNode4.Text = "ตั้งค่าหวย";
            treeNode5.ImageIndex = 15;
            treeNode5.Name = "Node4";
            treeNode5.SelectedImageIndex = 15;
            treeNode5.Tag = "RL001";
            treeNode5.Text = "บิลหวยแยกตามลูกค้า";
            treeNode6.ImageIndex = 16;
            treeNode6.Name = "Node0";
            treeNode6.SelectedImageIndex = 16;
            treeNode6.Tag = "RL002";
            treeNode6.Text = "สรุปบิลหวยทั้งหมดประจำงวด";
            treeNode7.ImageIndex = 32;
            treeNode7.Name = "Node0";
            treeNode7.SelectedImageIndex = 32;
            treeNode7.Tag = "LigorLottoNumber.Report.LottoReportForm;RL003";
            treeNode7.Text = "สรุปบิลหวยทั้งหมดแยกตามชุด";
            treeNode8.ImageIndex = 34;
            treeNode8.Name = "Node0";
            treeNode8.SelectedImageIndex = 34;
            treeNode8.Tag = "LigorLottoNumber.Report.LottoReportForm;RL005";
            treeNode8.Text = "สรุปบิลหวยทั้งหมดแยกตามชุดเรียงตามวันที่บันทึก";
            treeNode9.ImageIndex = 26;
            treeNode9.Name = "Node3";
            treeNode9.SelectedImageIndex = 26;
            treeNode9.Text = "รายงาน";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode4,
            treeNode9});
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(225, 440);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.MenuNodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Calculator.png");
            this.imageList1.Images.SetKeyName(1, "Counter.png");
            this.imageList1.Images.SetKeyName(2, "Monitor.png");
            this.imageList1.Images.SetKeyName(3, "Blue Lego.png");
            this.imageList1.Images.SetKeyName(4, "Home.png");
            this.imageList1.Images.SetKeyName(5, "Soccer Ball.png");
            this.imageList1.Images.SetKeyName(6, "Yellow Lego.png");
            this.imageList1.Images.SetKeyName(7, "Door.png");
            this.imageList1.Images.SetKeyName(8, "Green Chalkboard.png");
            this.imageList1.Images.SetKeyName(9, "Library Occupied.png");
            this.imageList1.Images.SetKeyName(10, "Spam Mail.png");
            this.imageList1.Images.SetKeyName(11, "Bonsai.png");
            this.imageList1.Images.SetKeyName(12, "Tree.png");
            this.imageList1.Images.SetKeyName(13, "Calendar.png");
            this.imageList1.Images.SetKeyName(14, "Clipped ID.png");
            this.imageList1.Images.SetKeyName(15, "Contacts.png");
            this.imageList1.Images.SetKeyName(16, "Contacts-alt.png");
            this.imageList1.Images.SetKeyName(17, "Document.png");
            this.imageList1.Images.SetKeyName(18, "Folder.png");
            this.imageList1.Images.SetKeyName(19, "Highlighter.png");
            this.imageList1.Images.SetKeyName(20, "ID.png");
            this.imageList1.Images.SetKeyName(21, "Manual.png");
            this.imageList1.Images.SetKeyName(22, "Movies.png");
            this.imageList1.Images.SetKeyName(23, "Music.png");
            this.imageList1.Images.SetKeyName(24, "Note.png");
            this.imageList1.Images.SetKeyName(25, "Note-Add.png");
            this.imageList1.Images.SetKeyName(26, "Notepad.png");
            this.imageList1.Images.SetKeyName(27, "Note-Remove.png");
            this.imageList1.Images.SetKeyName(28, "Pencil.png");
            this.imageList1.Images.SetKeyName(29, "Preferences.png");
            this.imageList1.Images.SetKeyName(30, "Text Edit.png");
            this.imageList1.Images.SetKeyName(31, "Add Square.png");
            this.imageList1.Images.SetKeyName(32, "Done Square.png");
            this.imageList1.Images.SetKeyName(33, "Key.png");
            this.imageList1.Images.SetKeyName(34, "Maximize Square.png");
            this.imageList1.Images.SetKeyName(35, "OK.png");
            this.imageList1.Images.SetKeyName(36, "RSS.png");
            // 
            // buttonItem1
            // 
            this.buttonItem1.Checked = true;
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.OptionGroup = "navBar";
            this.buttonItem1.Text = "เลขรางวัล";
            // 
            // lottoMasterTableAdapter1
            // 
            this.lottoMasterTableAdapter1.ClearBeforeFill = true;
            // 
            // lottoItemTableAdapter1
            // 
            this.lottoItemTableAdapter1.ClearBeforeFill = true;
            // 
            // MainParentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 558);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainParentForm";
            this.Text = "โปรแกรมบัญชีตัวเลข";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainParentForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPanePanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private DevComponents.DotNetBar.NavigationPane navigationPane1;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel1;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem จดการฐานขอมลToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เรมงวดใหมลางขอมลทงหมดToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ลางขอมลลกคาทงหมดToolStripMenuItem;
        private LigorLottoDataSetTableAdapters.LottoMasterTableAdapter lottoMasterTableAdapter1;
        private LigorLottoDataSetTableAdapters.LottoItemTableAdapter lottoItemTableAdapter1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ตงรหสผานToolStripMenuItem;
    }
}



