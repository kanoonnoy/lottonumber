﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Admin
{
    public partial class AdminSettingForm : Form
    {
        private Base.TableEntity table = new Base.TableEntity();
        public AdminSettingForm()
        {
            InitializeComponent();
            labelX4.Text = string.Empty;
            textBoxUserName.Text = Environment.MachineName;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            labelX4.Text = string.Empty;
            if (textBoxPassword.TextLength == 0 || textBoxNewPass1.TextLength == 0)
                return;
            

            if(textBoxNewPass1.Text != textBoxNewPass2.Text)
            {
                labelX4.Text = "รหัสผ่านไม่ตรงกันค่ะ";
                textBoxPassword.Clear();
                textBoxNewPass1.Clear();
                return;
            }

            if(textBoxNewPass1.TextLength < 4)
            {
                labelX4.Text = "รหัสผ่านอย่างน้อย 6 ";
                textBoxNewPass1.Clear();
                textBoxNewPass2.Clear();
                return;
            }

            if(textBoxNewPass1.Text == textBoxPassword.Text)
            {
                labelX4.Text = "รหัสผ่านต้องไม่ตรงกับรหัสผ่านเก่าค่ะ";
                textBoxNewPass1.Clear();
                textBoxNewPass2.Clear();
                return;
            }

            // Save NewPassword
            bool isChange = false;
            string str = string.Format("SELECT * FROM Admin WHERE Password = '{0}'", textBoxPassword.Text);
            table.Source = Base.TableEntity.Database.LigorLotto;
            if(table.SQLQuery(str))
            {
                if(table.Records.Rows.Count > 0)
                {
                    isChange = true;
                }
            }

            if(isChange)
            {
                table.Records.Rows[0]["Password"] = textBoxNewPass1.Text;
                int UpdateCount = this.adminTableAdapter1.Update(table.Records.Rows[0]);
                if(UpdateCount > 0)
                {
                    MessageBox.Show("บันทึกรหัสผ่านใหม่เรียบร้อยแล้วค่ะ","ตั้งค่ารหัสผ่าน",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                labelX4.Text = "รหัสผ่านเก่าไม่ถูกต้องกรุณาลองอีกครั้งค่ะ";
            }
        }

        private void textBoxPassword_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void textBoxNewPass2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                buttonX1_Click(this, new EventArgs());
        }
    }
}
