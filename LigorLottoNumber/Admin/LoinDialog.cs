﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LigorLottoNumber.Admin
{
    public partial class LoinDialog : Form
    {
        private Base.TableEntity table = new Base.TableEntity();
        public LoinDialog()
        {
            InitializeComponent();
            textBoxUserName.Text = Environment.MachineName;
        }

        private void textBoxX1_KeyUp(object sender, KeyEventArgs e)
        {

        }
        

        private bool Login(string password="")
        {
            bool isSuccess = false;
            
            try
            {
                string str = string.Format("SELECT * FROM Admin WHERE Password = '{0}'",password);
                table.Source = Base.TableEntity.Database.LigorLotto;
                if (table.SQLQuery(str))
                {
                    if(table.Records.Rows.Count > 0)
                    {
                        // Password correct
                        isSuccess = true;
                    }
                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
            
            return isSuccess;
        }
        private void textBoxX2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                buttonX1_Click(this, new EventArgs());
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (textBoxUserName.TextLength == 0 || textBoxPassword.TextLength == 0) return;
            if (Login(textBoxPassword.Text))
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {

                MessageBox.Show("รหัสผ่านไม่ถูกต้องกรุณาลองใหม่อีกครั้งค่ะ","เข้าสู่ระบบ",MessageBoxButtons.OK,MessageBoxIcon.Information);
                textBoxPassword.Clear();
                textBoxPassword.SelectAll();
            }
        }
    }
}
