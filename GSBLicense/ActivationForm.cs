﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GSBLicense
{
    public partial class ActivationForm : Form
    {
        License lc = new License();
        public string hdID { set { textBox1.Text = value; } }
        public string ProductKey { get { return textBox2.Text; } }

        public ActivationForm()
        {
            InitializeComponent();
        }

        private void btok_Click(object sender, EventArgs e)
        {
            string key = textBox2.Text;
            if (lc.MakeKey(textBox1.Text,key))
            {
                MessageBox.Show("ขอบคุณที่ลงทะเบียนผลิตภัณฑ์","ลงทะเบียน",MessageBoxButtons.OK,MessageBoxIcon.Information);
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                
            }
            else
            {
                MessageBox.Show("หมายเลขผลิตภัณฑ์ไม่ถูกต้อง กรุณาลองอีกครั้งค่ะ", "ลงทะเบียน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
    }
}
